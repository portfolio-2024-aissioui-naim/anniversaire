﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TpAdo
{
    class Anniversaire
    {
        private int id;
        private string nom;
        private string prenom;
        private string prenomPseudonyme;
        private string nomPseudonyme;
        private DateTime dateAnniversaire;
        private DateTime anniversaire;

        

        public DateTime GetdateAnniversaire()
        {
            return this.dateAnniversaire;
        }

        public int GetId()
        {
            return this.id;
        }

        public string GetNom()
        {
          return this.nom;
        }

        public string GetnomPseudonyme()
        {
            return this.nomPseudonyme;
        }

        public string GetprenomPseudonyme()
        {
            return this.prenomPseudonyme;
        }

        public Anniversaire(DateTime anniversaire, string prenomPseudonyme, string nomPseudonyme, string prenom, string nom)
        {
            this.anniversaire = anniversaire;
            this.prenomPseudonyme = prenomPseudonyme;
            this.nomPseudonyme = nomPseudonyme;
            this.prenom = prenom;
            this.nom = nom;
        }

        public new string ToString()
        {
            return string.Format("Le nom : {0}, le prenom {1}et la date de naissance : {2}", this.nomPseudonyme, this.prenomPseudonyme, this.dateAnniversaire);
        }

    }
}
