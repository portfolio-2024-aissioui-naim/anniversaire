﻿using System;
using TpAdo;

namespace TpAdo
{
    class Program
    {
        static void Main(string[] args)
        {

            /*Console.WriteLine( "Jour : ");
            int jour = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Mois : ");
            int mois = Convert.ToInt32(Console.ReadLine());

            TableAnniversaire ta = new TableAnniversaire();
            List<Anniversaire> resultat = ta.QuiEstNeCeJour(jour, mois);

            foreach(Anniversaire a in resultat)
            {
                Console.WriteLine(a.GetnomPseudonyme());
                Console.WriteLine(a.GetprenomPseudonyme());
                Console.WriteLine(a.GetdateAnniversaire());
            }*/

            //personne plus âgées
            Console.WriteLine("Saisissez un âge : ");
            int ageSaisi = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Personnes plus âgées que " + ageSaisi + " ans :");
            int ageCalcul = Convert.ToInt32(Console.ReadLine());


            TableAnniversaire ta = new TableAnniversaire();
            List<Anniversaire> personnesPlusAgees = ta.PersonnesPlusAgeesQue(ageSaisi);

            
            foreach (Anniversaire personne in personnesPlusAgees)
            {
                Console.WriteLine(personne.ToString());
                
            }

            /*TableAnniversaire tableAnniversaire = new TableAnniversaire();
            Console.WriteLine("Il y a {0} occurences dans la table anniversaire",
                tableAnniversaire.CompteOccurence());*/
        }
    }
    
}